from odoo import models,fields,api,_
from odoo.exceptions import ValidationError


def _hour_list():
    h = []
    for x in range(0, 24):
        if(x < 10):
            x = "0" + str(x)
        h.append((str(x),str(x)))
    return h

def _minute_list():
    m = [] 
    for x in range(0, 60):
        if(x < 10):
            x = "0" + str(x)
        m.append((str(x),str(x)))
    return m

class Clinic(models.Model):
    _name = 'clinic.clinic'
    _description = 'Clinic master data'

    name = fields.Char(string='Name', required=1)

class DoctorSchedule(models.Model):
    _name = 'clinic.doctor_schedule'
    _rec_name = 'doctor_id'
    _sql_constraints = [
        ('doctor_uniq', 'UNIQUE (doctor_id)', 'This Doctor already has schedule')
    ]
    doctor_id = fields.Many2one('res.users',string='Doctor',required=1,domain=[('doctor','=',True)])
    schedule_ids = fields.One2many('clinic.doctor_schedule.lines','schedule_id',string='Schedules',required=1)

class DoctorScheduleLine(models.Model):
    _name = 'clinic.doctor_schedule.lines'

    _DAYS = [
        ('1','Mon'),
        ('2','Tue'),
        ('3','Wed'),
        ('4','Thu'),
        ('5','Fri'),
        ('6','Sat'),
        ('7','Sun'),
    ]

    _HOURS = _hour_list()
    _MINUTES = _minute_list()


    schedule_id = fields.Many2one('clinic.doctor_schedule',string='Schedule',required=1,ondelelte='cascade')
    day_name =  fields.Selection(_DAYS,string='Day',required=1)
    hour_start = fields.Selection(_HOURS,string='Hour Start', required=1)
    minute_start = fields.Selection(_MINUTES,string='Minute Start', required=1)
    hour_end = fields.Selection(_HOURS,string='Hour End', required=1)
    minute_end = fields.Selection(_MINUTES,string='Minute End',required=1)

    

    @api.constrains('hour_start','hour_end','minute_start','minute_end')
    def _constraint(self):
        for s in self:
            hs, he, ms, me = int(s.hour_start), int(s.hour_end), int(s.minute_start), int(s.minute_end)
            if (hs or he) > 23 or (hs or he) < 0:
                raise ValidationError("Please, fill the time with proper hour!")

            if (ms or me) > 59 or (ms or me) < 0:
                raise ValidationError("Please, fill the time with proper hour!")

            if hs > he:
                raise ValidationError("Hour end must be greater than hour start!")

            if hs == he:
                if ms > me:
                   raise ValidationError("Hour end must be greater than hour start!")
