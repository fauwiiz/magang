from odoo import models,fields,api,_

class Doctor(models.Model):
    _inherit = 'res.partner'

    doctor = fields.Boolean(string='is A Doctor?')

class Partner(models.Model):
    _inherit = 'res.partner'

    _BLOOD = [
        ('a','A'),
        ('b','B'),
        ('o','O'),
        ('ab','AB')
    ]

    _GENDER = [
        ('male','Male'),
        ('female','Female')
    ]

    patient = fields.Boolean(string='is a Patient?')
    gender = fields.Selection(_GENDER,string='Gender')
    insurance = fields.Boolean(string='is a Insurance Company?')
    born = fields.Date(string='Date of Birth')
    blood = fields.Selection(_BLOOD, string='Blood Group')
