from odoo import models, fields, api
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DT
from datetime import datetime
import time
import logging


class Appt(models.Model):
    _name = 'clinic.appt'  # nama table di postgres clinic_appt
    _description = 'Modul untuk pendaftaran pasien'
    _order = "date ASC"

    @api.multi
    def action_confirm(self):
        self.state = 'confirm'
        self.name = self.env['ir.sequence'].next_by_code('clinic.appt')

    @api.multi
    def action_done(self):
        self.state = 'done'

    @api.multi
    def action_ongoing(self):
        self.state = 'ongoing'

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'

    @api.multi
    def action_payment(self):
        # membuat invoice
        inv_obj = self.env['account.invoice']

        inv_vals = {}
        inv_vals['type'] = 'out_invoice'  # invoice penjualan

        inv_lines = []

        for s in self:
            inv_vals['date_invoice'] = s.date
            inv_vals['partner_id'] = s.insurance_id and s.insurance_id.id or s.partner_id.id
            inv_vals['name'] = s.name
            inv_vals['comment'] = s.insurance_id and 'A/N %s' % (
                s.partner_id.name) or ''
            for l in s.line_ids:
                account_id = l.product_id.property_account_income_id.id
                logging.info("=== ACCCOUNT %s" % (account_id))
                inv_lines.append((0, 0,
                                  {'product_id': l.product_id.id,
                                   'name': l.name,
                                   'quantity': l.qty,
                                   'price_unit': l.price,
                                   'account_id': account_id
                                   }))
        if not inv_lines:
            raise ValidationError("Mohon isi data jasa/barang!")

        logging.info("==== INV LINES == %s" % (inv_lines,))
        inv_vals['invoice_line_ids'] = inv_lines
        logging.info("======= INVOICE ===== %s" % (inv_vals,))
        invoice = inv_obj.create(inv_vals)
        # logging.info(invoice)
        self.state = 'payment'
        self.invoice_id = invoice.id
        self._create_delivery()

    def _create_delivery(self):
        picking_obj = self.env['stock.picking']
        picking_type_id = self.env.ref('stock.picking_type_out')
        picking_type = self.env['stock.picking.type'].browse(
            picking_type_id.id)
        for s in self:
            picking_vals = {}
            logging.info("===== PICKING TYPE ==== %s" % (picking_type_id))
            picking_vals['picking_type_id'] = picking_type_id.id
            picking_vals['location_id'] = picking_type.default_location_src_id.id
            picking_vals['location_dest_id'] = s.partner_id.property_stock_customer.id
            picking_vals['name'] = s.name
            picking_vals['origin'] = s.name
            picking_vals['partner_id'] = s.partner_id.id
            products = []
            for l in s.line_ids:
                if l.product_id.type == 'product':
                    products.append((0, 0, {
                        'name': l.name,
                        'product_id': l.product_id.id,
                        'product_uom_qty': l.qty,
                        'quantity_done': l.qty,
                        'product_uom': l.product_id.uom_id.id
                    }))
            picking_vals['move_lines'] = products
            if len(products) > 0:
                # bikilah DO
                picking = picking_obj.create(picking_vals)
                s.picking_id = picking.id

    # @api.one
    # @api.depends('partner_id')
    # def _get_age(self):
    #     age = False
    #     if self.partner_id:
    #         age = datetime.now().year - datetime.strptime(self.partner_id.born, '%Y-%m-%d').year
    #     self.age = age

    @api.multi
    def unlink(self):
        if self.state != 'draft':
            raise ValidationError("Only draft that can be deleted!")
        return super(Appt, self).unlink()

    @api.multi
    @api.depends('line_ids.subtotal')
    def _compute_all(self):
        for s in self:
            if s.line_ids:
                s.grand_total = sum(d.subtotal for d in s.line_ids)

    name = fields.Char(string='Code', required=1,
                       readonly=True, default='/')
    partner_id = fields.Many2one('res.partner',
                                 readonly=True, states={'draft': [(
                                     'readonly', False)]}, string='Patient', required=True, domain=[('patient', '=', True)])
    date = fields.Datetime(string='Date', readonly=True, states={
                           'draft': [('readonly', False)]}, required=True, default=time.strftime(DT))
    clinic_id = fields.Many2one('clinic.clinic', readonly=True, states={
                                'draft': [('readonly', False)]}, string='Clinic', required=True)
    doctor_id = fields.Many2one('res.users', readonly=True, states={'draft': [(
        'readonly', False)]}, string='Doctor', domain=[('doctor', '=', True)])
    inscription = fields.Html(string='Inscription', readonly=True, states={
                              'ongoing': [('readonly', False)]})
    invoice_id = fields.Many2one(
        'account.invoice', string='Invoice', readonly=True)
    picking_id = fields.Many2one(
        'stock.picking', string='Delivery Order', readonly=True)
    insurance_id = fields.Many2one('res.partner', string='Insurance', domain=[(
        'insurance', '=', True)], readonly=True, states={'draft': [('readonly', False)]})
    # age = fields.Integer(_get_age, string="Age")
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('confirm', 'Confirmed'),
            ('cancel', 'Cancelled'),
            ('ongoing', 'On Going'),
            ('payment', 'Payment'),
            ('done', 'Done'),
        ],
        string='Status',
        required=1,
        readonly=1,
        default='draft')
    line_ids = fields.One2many('clinic.appt.line', 'appt_id', readonly=True, states={
                               'ongoing': [('readonly', False)]}, string='Service/Item')
    grand_total = fields.Float(
        string='Grand Total', compute='_compute_all', store=True)


class AptSoap(models.Model):
    _inherit = 'clinic.appt'

    subjective = fields.Text(string='Subjective')
    objective = fields.Text(string='Objective')
    assesment = fields.Text(string='Assesment')
    plan = fields.Text(string='Plan')


class AptLine(models.Model):
    _name = 'clinic.appt.line'

    @api.multi
    @api.depends('qty', 'price')
    def _compute_all(self):
        for s in self:
            if s.qty or s.price:
                s.subtotal = s.qty * s.price

    @api.onchange('product_id')
    def onchange_product(self):
        if self.product_id:
            self.name = self.product_id.name
            self.price = self.product_id.lst_price

    name = fields.Char(string='Name', required=1)
    product_id = fields.Many2one('product.product', string='Item', domain=[
                                 ('clinic_item', '=', True)])
    qty = fields.Float(string='Qty', default=1.0)
    price = fields.Float(string='Price')
    subtotal = fields.Float(
        string='Sub Total', compute='_compute_all', readonly=1, store=True)
    appt_id = fields.Many2one(
        'clinic.appt', string='Appointment', required=1, ondelete='cascade')
